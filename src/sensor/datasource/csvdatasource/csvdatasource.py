
from pyflink.table import types
from pyflink.table import CsvTableSource


class CsvDatasource:
    def __init__(self):
        self.dataset_path=None

    def getCsvSource(self) -> CsvTableSource:
        return CsvTableSource(
            source_path=self.dataset_path,
            ignore_first_line=True,
            field_names = [f"Sensor-{k}" for k in range(1, 11)],
            field_types = [types.VarCharType()]*10
        )
