from pyflink.common.serialization import Encoder
from pyflink.common.typeinfo import Types
from pyflink.datastream import StreamExecutionEnvironment
from pyflink.datastream.connectors import StreamingFileSink
from job.job import AnomalyDetectionJob
def main():
    anomaly_detection_job = AnomalyDetectionJob()
    print("Hello from the main")

if __name__ == '__main__':
    main()
    
